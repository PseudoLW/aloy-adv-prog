package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
        description = "cheese";
    }

    @Override
    public String getDescription() {
        return String.format("%s, adding %s", 
                food.getDescription(), description);
    }

    @Override
    public double cost() {
        return food.cost() + 2.0;
    }
}
