package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
        description = "tomato";
    }

    @Override
    public String getDescription() {
        return String.format("%s, adding %s", 
                food.getDescription(), description);
    }

    @Override
    public double cost() {
        return food.cost() + 0.5;
    }
}
